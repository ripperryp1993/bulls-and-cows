from new_function import (convert_to_list, generate_secret, 
exact_match, common_match, convert_to_str)


def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    digits = input("How many digits would you like to set?\n")
    digits = int(digits)
    print("Can you guess it?")
    turns = input("How many turns would you like to try?\n")
    turns = int(turns)
    print()



    secret = generate_secret(digits)
    

    for i in range(turns):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # TODO:
        # while the length of their response is not equal
        # to 4,
        #   tell them they must enter a four-digit number
        #   prompt them for the input, again
   
        while len(guess) != digits:
            print('Sorry, you must enter a four-digit number!')
            prompt = "Type in guess #" + str(i + 1) + ": "
            guess = input(prompt)
        
        # TODO:
        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers
        
        guess_list = convert_to_list(guess)
        
        # TODO:
        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secret
        
        bulls_num = exact_match(secret, guess_list)
    
        # TODO:
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret
        
        cows_num = common_match(secret, guess_list)
     
        # TODO:
        # if the number of exact matches is four, then
        # the player has correctly guessed! tell them
        # so and return from the function.

        if (bulls_num == digits):
            print('Congrats! You Win!\nThe number is', 
            convert_to_str(guess_list))
            return
        else:
            print("Sorry, that's not correct.")
        
        # TODO:
        # report the number of "bulls" (exact matches)

            print('There are', bulls_num, 'bulls.')

        # TODO:
        # report the number of "cows" (common entries - exact matches)
            
            print('There are', cows_num, 'cows.')        
    
    # TODO:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.

    print("Sorry, you did't get it. The secret number is:",
    convert_to_str(secret))

# Runs the game
def run():
    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    
    response = input('Do you want to play a game?\n Y or N\n')
    while response != 'Y' and response != 'N':
        response = input('Sorry, I cannot recognize that,' + 
        'please try again.\nY or N\n')
    while(response == 'Y'):
        play_game()
        response = input('Do you want to play again?\n Y or N\n')

if __name__ == "__main__":
    run()
