from random import randint


# Generates a list of four, unique, single-digit numbers
def generate_secret(digits):
    secret_number = []
    while len(secret_number) < digits:
            secret_number.append(randint(0,9))
    return secret_number

def convert_to_list(string):
    converted_list = []
    for value in string:
        converted_list.append(int(value))
    return converted_list

def exact_match(first, second):
    matches = 0
    for a, b in zip(first, second):
        if a == b:
            matches += 1
    return matches

def common_match(first, second):
    matches = 0
    hashmap1 ={}
    hashmap2 ={}
    for a in first:
        if (a in hashmap1.keys()):
            hashmap1[a] += 1
        else:
            hashmap1[a] = 1
    for b in second:
        if (b in hashmap2.keys()):
            hashmap2[b] += 1
        else:
            hashmap2[b] = 1
    for key in hashmap1:
        if ( key in hashmap2):
            matches += min(hashmap1[key], hashmap2[key])
    matches -= exact_match(first, second)
    return matches


def convert_to_str(alist):
    result_str = ''
    for value in alist:
        result_str += str(value)
    return result_str

